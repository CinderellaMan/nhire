{-# LANGUAGE OverloadedStrings #-}
module OrdersProducers (orders) where

import Pipes.Csv (decode)
import Pipes.ByteString (stdin, ByteString)
import Data.Csv ((.:), FromRecord(..), Record)
import Pipes
import Control.Applicative
import Models (Order)

instance FromRecord Order where
  parseRecord v =
    Order <$> v .! 0
          <$> v .! 1
          <$> v .! 2
          <*> v .! 3

orders :: Monad m
        => Producer ByteString m ()
        -> Producer (Either String Order) m ()

orders = decode NoHeader