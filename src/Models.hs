module Models
    ( Order, Currency, Direction
    ) where

data Order = Order Int Currency Int Direction deriving (Show) 
data Currency = GBP | USD | EUR deriving (Show) 
data Direction = BUY | SELL deriving (Show) 
