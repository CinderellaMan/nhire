module OrdersProducers (replayCsv) where

import Pipes
import Pipes.Prelude as P
import System.IO

replayCsv :: FilePath -> Producer String IO ()
replayCsv file = do
    h <- lift $ openFile file ReadMode
    fromHandle h
    lift $ hClose h