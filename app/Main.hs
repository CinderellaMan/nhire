module Main where

import Pipes
import Pipes.Prelude as P
import OrdersProducers (replayCsv)

main :: IO ()
main = runEffect $ replayCsv "data/market_a.csv" >-> P.take 3 >-> stdoutLn
